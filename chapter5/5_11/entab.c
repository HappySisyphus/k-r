#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define BUFFER_SIZE 1000
void entab(char* input, char* output, int tabSpace);
int main(int argc, char *argv[]){
  
  int tabSpace;
  size_t bufferSize = 1000;
  char *inputBuffer, *outputBuffer;
  inputBuffer = (char *)malloc(BUFFER_SIZE*sizeof(char));
  outputBuffer = (char *)malloc(BUFFER_SIZE*sizeof(char));
  if(argc > 1){
    tabSpace = atoi(*(argv+1));
  }
  else 
    tabSpace = 4;
  




  while(getline(&inputBuffer,&bufferSize, stdin) && *inputBuffer!=EOF){
  entab(inputBuffer,outputBuffer,tabSpace);
  printf("%s\n", outputBuffer);}
  free(inputBuffer);
  free(outputBuffer);
  return 0;



}

void entab(char* input, char* output, int tabSpace){

  int c;
  int blankCount = 0;
  int length = strlen(input);
  for(int i = 0; i<length; i++){
    c = *input++;
    if(c == ' '){
      while(c == ' '){
        ++blankCount;
        c = *input++;
        if(blankCount == tabSpace){
          *output++ = '-';
          blankCount = 0;
        }
      }
      for(int j = 0; j<blankCount; j++){
        *output++ = ' ';
      }
    }  
    *output++ = c;
}}





