/*
* @Author: simonvonschmalensee
* @Date:   2018-07-18 12:11:39
* @Last Modified by:   simonvonschmalensee
* @Last Modified time: 2018-07-18 17:40:37
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
void detab(char* input, int tabStops[], char* output);
int tabStops[10];
char inputBuffer[1000] = "F\tO\tO";
char outputBuffer[1000];
int main(int argc, char* argv[]){
	int j = 0;
	// Parse CL arguments
	for(int i = 1; i<argc; i++,j++){
		tabStops[j] = atoi(*(argv+i));}

	detab(inputBuffer,tabStops,outputBuffer);
	printf("%s\n", outputBuffer);




}

void detab(char* input,int tabStops[], char* output){
	int tabStopIndex = 0;
	int outputPosition = 0;
	int length = strlen(input);

	for(int inputPosition = 0; inputPosition<length; inputPosition++){
		if(*input == '\t' && tabStops[tabStopIndex]>outputPosition){
			for(int k = outputPosition; k<tabStops[tabStopIndex]; k++){
				*output++ = ' ';
				outputPosition++;
			}
			input++;
			tabStopIndex++;}
		else{
			*output++ = *input++;
			outputPosition++;
		}
	} 

}