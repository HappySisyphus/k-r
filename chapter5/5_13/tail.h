#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int readLines1(char *linePointers[], int maximumNoOfLines, char lineBuffer[],
               int bufferSize);
void writeLines(char *linePointers[], int numberOfLines, int linesToPrint);
