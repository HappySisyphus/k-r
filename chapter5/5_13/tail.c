#include "tail.h"
#define MAX_LINE_LENGTH 1000 /*max length for a input line*/
int getline1(char *line, int max);



//Prints the linesToPrint last lines of the input. 
void writeLines(char *linePointers[], int numberOfLines, int linesToPrint) {
  for (int i = 0; i < linesToPrint; i++) {
    printf("%s\n", *(linePointers + (numberOfLines - linesToPrint + i)));
  }
}

int getline1(char *line, int max) {

  if (fgets(line, max, stdin) == NULL)
    return 0;
  else
    return strlen(line);
}

int readLines1(char *linePointers[], int maximumNoOfLines, char lineBuffer[],
               int bufferSize) {
  int nextFreePosition = 0, length, numberOfLines;
  char *newLinePointer;
  char tempLineBuffer[MAX_LINE_LENGTH];

  while ((length = getline1(tempLineBuffer, MAX_LINE_LENGTH)) > 0)
    if (numberOfLines >= maximumNoOfLines ||
        bufferSize < nextFreePosition + length)
      return -1;
    else {
      tempLineBuffer[length - 1] = '\0';
      newLinePointer = &lineBuffer[nextFreePosition];
      strcpy(newLinePointer, tempLineBuffer);
      linePointers[numberOfLines++] = newLinePointer;
      nextFreePosition += length;
    }

  return numberOfLines;
}
