#include "tail.h"
#define MAX_NO_LINES 5000
#define BUFFER_SIZE 10000

char allocationBuffer[BUFFER_SIZE]; // The storage
char *linePointers[MAX_NO_LINES];

int main(int argc, char *argv[]) {
  int linesToPrint;
  if (argc > 1) {
    linesToPrint = atoi(&argv[1][1]);
  }
  else
  	linesToPrint = 10;

  int numberOfLines;
  if ((numberOfLines = readLines1(linePointers, MAX_NO_LINES, allocationBuffer,
                                  BUFFER_SIZE)) >= 0) {
   	
  	if(linesToPrint>numberOfLines){
  		linesToPrint = numberOfLines;
  		printf("ERROR: The number of lines to print exceeds the lines in the file. The whole file will be printed\n");
  		printf("The file has %d lines\n", numberOfLines);
  	}
    writeLines(linePointers, numberOfLines, linesToPrint);
    return 0;
  } else {
    printf("ERROR");
    return 1;
  }
}
