
#include "getfloat.h"
int isScientific = FALSE;
int getfloat(double *pn){
	int c, exponent;	
	double sign, power,multiplier, multiplierExponent;
	multiplierExponent = 1;

	while((c = getch()) == ' ' || c == '\t')
		;
	

	if(!isdigit(c) && c !=EOF && c != '+' && c != '-'){
		ungetch(c);
		return 0;
	}
	
	sign = (c == '-')? -1.0 : 1.0;
	if(c == '+' || c == '-')
		c = getch();

	for(*pn = 0.0; isdigit(c); c = getch())
		*pn = 10.0 * *pn + (c - '0');
	if(c == '.')
		c =getch();

	for (power = 1.0; isdigit(c); c = getch()){
		*pn = 10.0 * *pn + (c - '0');
		power *= 10.0;}

	if(c == 'e' || c == 'E'){
		isScientific = TRUE;
		c = getch();
	}
	if(isScientific){
		multiplier = (c == '-') ? 0.1 : 10;
		if(c == '+' || c == '-')
			c = getch();
		for (exponent = 0; isdigit(c); c = getch()){
			exponent = 10 * exponent + (c - '0');
		}
		while(exponent>0){
			multiplierExponent *= multiplier;
			exponent--;
		}
	}

	*pn =  sign*(*pn/power)*multiplierExponent;

	if(c != EOF){
		ungetch(c);
	} 
	isScientific = FALSE; 

	return c;




} 