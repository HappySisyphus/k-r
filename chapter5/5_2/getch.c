#include "getfloat.h"


#define BUFFER_SIZE 100
char buffer[BUFFER_SIZE]; // Buffer to store the pushed back characters
int bufferPointer = 0;

int getch(void) {
  return (bufferPointer > 0) ? buffer[--bufferPointer] : getchar();
}
void ungetch(int c) {
    // 4-9 The EOF will be ignored
    //if(c != EOF){
	    if (bufferPointer == BUFFER_SIZE - 1) {
    	    printf("ERROR: BUFFER FULL\n");
  	    } 
  	    else{
  	    	printf("%c\n", c);
    	    buffer[bufferPointer++] = c;}
	//}	
}


