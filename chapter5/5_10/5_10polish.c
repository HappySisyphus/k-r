#include "5_11polish.h"

int main(int argc, char *argv[]) {
  char *c;
  int op2;
  initStack();
  while ((c = *(++argv))) {
    if (isdigit(*c)) {
      push(atoi(c));
    } else {
      switch (*c) {
      case '+':
        push(pop() + pop());
        break;
      case '-': 
      	op2 = pop();
      	push(pop()-op2);
      	break;
      case '*':
      	push(pop()*pop());
      	break;
      case '/':
      	if((op2 = pop())== 0)
      		printf("error: Division by zero");
      	else
      		push(pop() / op2);
      	break;
      default:
      	printf("Unkown Operand");
        break;
      }
    }
	}
    printf("%d\n", pop());
    return 0;
}
