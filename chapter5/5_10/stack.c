/*
* @Author: simonvonschmalensee
* @Date:   2018-07-18 08:17:40
* @Last Modified by:   simonvonschmalensee
* @Last Modified time: 2018-07-18 09:24:57
*/
#include <stdio.h>
#define STACK_SIZE 100
int stack[STACK_SIZE];
int *stackPointer;
int itemsInStack;

void initStack(){
	stackPointer = stack;
}

void push(int c){
	if(itemsInStack<STACK_SIZE){
		*(stackPointer) = c;
		itemsInStack++;
		stackPointer++;
	} 
	else
		printf("The stack is full");
}

int pop(void){
	if(itemsInStack < 1){
		printf("No items in the stack\n");
		return -1;
	}
	else{

		itemsInStack--;
		return *(--stackPointer);
	}

}