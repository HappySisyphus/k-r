/*cha
* @Author: simonvonschmalensee
* @Date:   2018-07-20 09:33:54
* @Last Modified by:   simonvonschmalensee
* @Last Modified time: 2018-07-21 11:01:37
*/
#include "parser.h"

int main(int argc, char *argv[]){
	int isPointer = 0;
	int type;
	char temp[MAX_LENGTH_OF_TOKEN];

	while(getToken()!=EOF){
		strcpy(output, token);
		while((type = getToken())!='\n'){
			if(type == PARENTHESES || type == BRACKETS ){
				if(isPointer){
					sprintf(temp, "(%s)", output);
					strcpy(output, temp);
					isPointer = 0;	
				}
				strcat(output, token);
				isPointer = 0;}
			else if (type == '*'){
				isPointer = 1;
				sprintf(temp, "*%s", output); 
				strcpy(output,temp);
				}
			
			else if (type == NAME){
				sprintf(temp, "%s, %s", token, output);
				strcpy(output, temp);
				isPointer = 0;
				
			}
			else
				printf("invalid input at %s\n", token);
		}
	
		printf("%s\n",output);

	}
		
return 0;}