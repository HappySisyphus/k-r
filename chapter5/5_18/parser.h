#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define MAX_LENGTH_OF_TOKEN 100
#define MAX_LENGTH_OF_OUTPUT 1000

// Function declarations
void dcl(void);
void dirdcl(void);
int getToken(void);
void clear(void);


// Global varibles
enum {NAME, PARENTHESES, BRACKETS};
int tokenType;
char token[MAX_LENGTH_OF_TOKEN];
char name[MAX_LENGTH_OF_TOKEN];
char dataType[MAX_LENGTH_OF_TOKEN];
char output[MAX_LENGTH_OF_OUTPUT];

