#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
int getch(void);
void ungetch(int c);
int isDigit(int c);
int getInt(int *pn);

int main(void){
	int *integer1;
	*integer1 = 1024;
	getInt(integer1);
	printf("%d\n", *integer1);
	char c = getch();
	printf("%c\n",c );
	c = getch();
	printf("%c\n",c );
	return 0;
}




int getInt(int *pn){

	int c, sign;

	//Skip whitespace
	while((c = getch()) == ' '|| c == '\t' )
		;

	

	if(!isdigit(c) && c!=EOF && c != '+' && c!='-'){
		ungetch(c); // Not a number
		return 0;} 

	sign = (c == '-') ? -1 : 1; 
	if(c == '+' || c == '-' ){

		int temp = getch();
		if(!(isdigit(temp))){
			ungetch(temp);
			ungetch(c);
			return 0;
		}
	 	c = temp;
	 }

	 for (*pn = 0; isdigit(c); c = getch()){
	 	*pn = 10 * *pn + (c -'0');}
	 
	 *pn*=sign;
	 if(c != EOF)
	 	ungetch(c);

	 
	return c;
}


int isDigit(int c){
	return (c <='9' && c>='0') ? 1 : 0;
}


// for getch and ungetch
#define BUFFER_SIZE 100
char buffer[BUFFER_SIZE]; // Buffer to store the pushed back characters
int bufferPointer = 0;

int getch(void) {
  return (bufferPointer > 0) ? buffer[--bufferPointer] : getchar();
}
void ungetch(int c) {
    // 4-9 The EOF will be ignored
    if(c != EOF){
	    if (bufferPointer == BUFFER_SIZE - 1) {
    	    printf("ERROR: BUFFER FULL\n");
  	    } 
  	    else{
  	    	printf("%c\n", c);
    	    buffer[bufferPointer++] = c;}
	}	
}








