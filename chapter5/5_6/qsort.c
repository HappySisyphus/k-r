#include "sortLines.h"
void swap(char *v[], int i, int j);

void qsort1(char *v[], int leftIndex, int rightIndex){

	int i, lastSwapped;

	//Basecase 
	if (leftIndex >= rightIndex)
		return;

	swap(v, leftIndex, (leftIndex + rightIndex)/2);
	lastSwapped = leftIndex;
	for (i = leftIndex+1; i<=rightIndex; i++)
		if(strcmp(v[i],v[leftIndex])<0)
			swap(v, ++lastSwapped,i);
	swap(v, leftIndex, lastSwapped);
	qsort1(v, leftIndex, lastSwapped-1);
	qsort1(v, lastSwapped+1, rightIndex);
}


void swap(char *v[], int i, int j){
	char *temp;
	temp = v[i];
	v[i] = v[j];
	v[j] = temp;
 }