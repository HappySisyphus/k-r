/*
* @Author: simonvonschmalensee
* @Date:   2018-07-17 18:25:28
* @Last Modified by:   simonvonschmalensee
* @Last Modified time: 2018-07-17 20:51:26
*/
#include <stdio.h>
static char daytab[2][13] = {
  {0,31,28,31,30,31,30,31,31,30,31,30,31},
  {0,31,29,31,30,31,30,31,31,30,31,30,31}
};



int dayOfYear(int year, int month, int day){
	int i, isLeap;
	isLeap = ((year%4 == 0) && (year%100 != 0)) || year%400 == 0;

	if(year<0 || month < 1 || month > 12 || daytab[isLeap][month] < day){
		return 0;
	}

	
	for( i = 1; i<month; i++)
		day += daytab[isLeap][i];
	
	return day; 
}

void monthDay(int year, int yearday, int *monthPointer, int *dayPointer){
	int month, isLeap;
	int dayOfMonth = 1;
	int i = 1; 
	month = 1;
	char* p;
	isLeap = ((year%4 == 0) && (year%100 != 0)) || year%400 == 0;
	if((isLeap && yearday > 357) || yearday > 356)
		return; 
	p = &daytab[isLeap]+2;
	
	while(i < yearday){
	 dayOfMonth++;
     if(dayOfMonth > *(p+month)){
		month++;
		dayOfMonth = 1;}
	  i++;}

	*monthPointer = 10;//month;
	*dayPointer =   20;//dayOfMonth;

}