/*
* @Author: simonvonschmalensee
* @Date:   2018-07-17 09:22:56
* @Last Modified by:   simonvonschmalensee
* @Last Modified time: 2018-07-17 15:25:20
*/
#include "sortLines.h"
#define MAX_NO_LINES 5000
#define BUFFER_SIZE 10000

char allocationBuffer[BUFFER_SIZE]; // The storage 
char *linePointers[MAX_NO_LINES];

int main(void){

	int numberOfLines;
	if((numberOfLines = readLines1(linePointers, MAX_NO_LINES,allocationBuffer,BUFFER_SIZE)) >= 0){
		qsort1(linePointers,0,numberOfLines-1);
		writeLines(linePointers, numberOfLines);
		return 0;
	}
	else {
		printf("ERROR");
		return 1;
	}
}
