#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

char *alloc(int numberOfChars);
void afree(char *memoryToBeFreed);
int readLines(char *linePointers[], int maximumNumberOfLines);
int readLines1(char *linePointers[], int maximumNoOfLines, char lineBuffer[], int bufferSize);
void writeLines(char *linePointers[], int numberOfLines);
void qsort1(char *v[], int leftIndex, int rightIndex);
