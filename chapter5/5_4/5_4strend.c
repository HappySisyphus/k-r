#include <stdio.h>


int strend(char *s,char *t);
int strlen1(char *s);

int main(void){
	char a[10] = "FOOLOL";
	char b[10] ="LOLLOL";
	printf("%d\n", strend(a,b));
	return 0;
}


int strend(char *s, char *t){
 	int sLength  = strlen1(s);
 	int tLength = strlen1(t);
 	int lengthDifference = sLength-tLength;
 	
 	if(lengthDifference) // Check if t is longer than s
 		return 0;

 	for(int k = 0; k<lengthDifference; k++){
 		if(*(s+lengthDifference+k) != *(t+k))
 			return 0;
 	}

 	return 1;
 }


int strlen1(char *s){
	int length;
	for(length = 0; *(s+length); length++);
	return length;
}