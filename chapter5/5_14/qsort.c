/*
 * @Author: simonvonschmalensee
 * @Date:   2018-07-19 09:14:00
 * @Last Modified by:   simonvonschmalensee
 * @Last Modified time: 2018-07-21 09:11:02
 */
#include "main.h"
void swap(void *v[], int i, int j);

	void qsort1(void *v[], int left, int right, int (*comp)(void *, void *),
 	           int increesingOrDecresing) {
  		int last, compare;
  		//Base case 
  		if (left >= right) {
    	return;
  		}

  		swap(v, left, (right + left) / 2);
  		last = left;
  		for (int i = left + 1; i <= right; i++) {
    		compare = (increesingOrDecresing) ? -(*comp)(v[i], v[left])
                                      	: (*comp)(v[i], v[left]);
    	if (compare < 0)
      		swap(v, ++last, i);
  		}
  		swap(v, last, left);
  		qsort1(v, left, last - 1, comp, increesingOrDecresing);
  		qsort1(v, last + 1, right, comp, increesingOrDecresing);
	}

void swap(void *v[], int i, int j) {

  void *temp = v[i];
  v[i] = v[j];
  v[j] = temp;
}

// Does not take case into consideration
// when comparing values.
	int strcmpFold(char *s1, char *s2) {
  		while (toupper(*s1) == toupper(*s2) && *s1) {
    		s1++;
    		s2++;
 		}
    
  		if (toupper(*s1) < toupper(*s2))
    		return -1;
  		else if (toupper(*s1) > toupper(*s2))
    		return 1;
  		else
    		return 0;
	}

	int numcmp(char *s1, char *s2) {
  		double v1, v2;
  		v1 = atof(s1);
  		v2 = atof(s2);

  		if (v1 < v2)
    		return -1;
  		else if (v1 > v2)
    		return 1;
  		else
    		return 0;
	}
