#include "main.h"
#define BUFFER_SIZE 10000

static char allocationBuffer[BUFFER_SIZE]; // The storage
static char *nextFreePosition = allocationBuffer; // Pointer the next free position

char *alloc(int numberOfChars) {

  if (allocationBuffer + BUFFER_SIZE - nextFreePosition >= numberOfChars) {
    nextFreePosition += numberOfChars;
    return nextFreePosition - numberOfChars;
  } else
    	return NULL;
}

void afree(char *memoryToBeFreed) {
	
	if (memoryToBeFreed >= allocationBuffer && nextFreePosition < allocationBuffer + BUFFER_SIZE)
  		nextFreePosition = memoryToBeFreed;
}
