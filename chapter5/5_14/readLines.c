#include "main.h"
#define MAX_LINE_LENGTH 1000 /*max length for a input line*/
int getline1(char *line, int max);
int readLines(char *linePointers[], int maximumNoOfLines){
	int length, numberOfLines;
	char *newLinePointer, *nextFreePosition, lineBuffer[MAX_LINE_LENGTH];

	numberOfLines = 0;
	while((length = getline1(lineBuffer, MAX_LINE_LENGTH)) > 0)
		if(numberOfLines >= maximumNoOfLines || (newLinePointer = alloc(length)) == NULL)
			return -1; 
		else {
			lineBuffer[length-1] = '\0'; //Remove '\n'
			strcpy(newLinePointer, lineBuffer);
			linePointers[numberOfLines++] = newLinePointer;
		}
	return numberOfLines;
}

void writeLines(char *linePointers[], int numberOfLines){
	
	while(numberOfLines-- > 0)
		printf("%s\n", *linePointers++); 
}

int getline1(char *line, int max){

	if(fgets(line, max, stdin) == NULL)
		return 0;
	else 
		return strlen(line);
}




