#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#define MAX_NO_OF_LINES 5000
#define COMPARE_DEFAULT 0
#define COMPARE_FOLDED 1
#define COMPARE_NUMERICLY 2

char *alloc(int numberOfChars);
void afree(char *memoryToBeFreed);
int readLines(char *linePointers[], int maximumNoOfLines);
void writeLines(char *linePointers[], int numberOfLines);

// qsort takes a list of pointers and sorts them
//The function for comparing two objects must be passed
// as an argument.
void qsort1(void *linePointers[], int left, int right,
	      int (*compare)(void *, void *), int increesingOrDecresing);

// Compares two strings numbericly.
int numcmp(char *, char *);
int strcmpFold(char *s1, char *s2);
