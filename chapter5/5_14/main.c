	#include "main.h"
	char *pointersToLines[MAX_NO_OF_LINES];
	int (*compareFunctionPointers[3])(void *, void *);
	int main(int argc, char *argv[]) {

  		// Store pointers to the diffrent types of
  		// compare functions in a array of
  		// function pointers.
  		compareFunctionPointers[COMPARE_DEFAULT] = &strcmp;
  		compareFunctionPointers[COMPARE_FOLDED] = &strcmpFold;
  		compareFunctionPointers[COMPARE_NUMERICLY] = &numcmp;

  		int numberOfLines;
  		int sortNumericly = 0;    // 1 if the input is to be sorted numericly
  		int sortDecreseingly = 0; // 1 if the input is to be sorted decreseingly
  		int compareFunction = COMPARE_DEFAULT; // Index for the compare-function
  		// Parse the command line arguments
  		if (argc > 1) {
    		for (int i = 1; *(argv + i); i++) {
      			char *c = *(argv + i);
      			if(strcmp(c, "-n") == 0) {
        			sortNumericly = 1;
        			compareFunction = COMPARE_NUMERICLY;
      			}
      			if(strcmp(c, "-r") == 0) 
        			sortDecreseingly = 1;
        		if(strcmp(c, "-f"))
          			compareFunction = COMPARE_FOLDED;
        		
      			
    		}
  		}

  		if ((numberOfLines = readLines(pointersToLines, MAX_NO_OF_LINES)) >= 0) {
    		qsort1((void **)pointersToLines, 0, numberOfLines - 1,
       	 	   (int (*)(void *, void *))compareFunctionPointers[compareFunction],
           		sortDecreseingly);

    		writeLines(pointersToLines, numberOfLines);
    		return 0;
  		}
	}