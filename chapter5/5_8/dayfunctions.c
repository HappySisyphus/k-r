/*
* @Author: simonvonschmalensee
* @Date:   2018-07-17 18:25:28
* @Last Modified by:   simonvonschmalensee
* @Last Modified time: 2018-07-18 08:16:07
*/
#include <stdio.h>
static char daytab[2][13] = {
  {0,31,28,31,30,31,30,31,31,30,31,30,31},
  {0,31,29,31,30,31,30,31,31,30,31,30,31}
};



int dayOfYear(int year, int month, int day){
	int i, isLeap;
	isLeap = ((year%4 == 0) && (year%100 != 0)) || year%400 == 0;

	if(year<0 || month < 1 || month > 12 || daytab[isLeap][month] < day){
		return 0;
	}

	
	for( i = 1; i<month; i++)
		day += daytab[isLeap][i];
	
	return day; 
}

void monthDay(int year, int yearday, int *monthPointer, int *dayPointer){
	int month, isLeap, dayOfMonth;
	printf("%d\n",month);

	printf("%d\n",isLeap);
	int i = 0; 
	dayOfMonth = 0;
	month = 1;
	char* p;
	
	isLeap = ((year%4 == 0) && (year%100 != 0)) || year%400 == 0;
	if((isLeap && yearday > 357) || yearday > 356 || yearday <= 0)
	{	printf("FOO\n");
		return; 
	}
	p = daytab[isLeap];
	while(i < yearday){
	
	 dayOfMonth++;
     if(dayOfMonth > *(p+month)){
		month++;
		dayOfMonth = 1;}
	  i++;} 

	*monthPointer = month;
	*dayPointer =   dayOfMonth;

}