#include <stdio.h>
void qsort(int v[], int left, int right);
int main(void){
	int array[10] = {1,53,19,482,281,19,12,1,54,2};
	qsort(array,0,9);
	for(int i = 0; i<10; i++){
		printf("%d\t",array[i]);
	}
}



void qsort(int v[],int left ,int right){
	int i, last;
	void swap(int v[],int i, int j);
	
	//BASE CASE 
	if(left >= right) // If the array contains less then two elements then return
		return;
	swap(v, left, (left + right)/2);
	last = left;
	for(i = left+1; i<= right; i++)
			if(v[i]<v[left])
				swap(v, ++last,i);
	swap(v,left, last);
	qsort(v,left,last-1);
	qsort(v,last+1,right);
}



void swap(int v[], int i, int j){
	int temp = v[i];
	v[i] = v[j];
	v[j] = temp;
}