#include <stdio.h>
#include "calculator.h"
#define STACK_SIZE 100
int stackPointer = 0;
double stack[STACK_SIZE];

void push(double f) {
  if (stackPointer < STACK_SIZE) {
    stack[stackPointer++] = f;
  } else
    printf("Stack is full!\n");
}

double pop(void) {
  if (stackPointer > 0) {
    return stack[--stackPointer];
  } else
    printf("Stack is empty\n");
  return 0.0;
}

int isEmpty() { return stackPointer; }

// For 4-4
void swapTopElements(void) {
  if (stackPointer > 1) {
    double temp1 = pop();
    double temp2 = pop();
    push(temp1);
    push(temp2);
  } else
    printf("Not enough elements in the stack\n");
}

void duplicate(void) {
  double temp = pop();
  push(temp);
  push(temp);
}

void clear(void) { stackPointer = 0; }

double topElement(void) {
  if (stackPointer > 0)
    return stack[stackPointer - 1];

  else {
    printf("Stack is empty\n");
    return 0.0;
  }
}