#include <stdio.h>
#include <stdlib.h>
#define BUFFER_SIZE 100
char buffer[BUFFER_SIZE]; // Buffer to store the pushed back characters
int bufferPointer;

int getch(void) {
  return (bufferPointer > 0) ? buffer[--bufferPointer] : getchar();
}

void ungetch(int c) {
    // 4-9 The EOF will be ignored
    if(c != EOF){
	    if (bufferPointer == BUFFER_SIZE - 1) {
    	    printf("ERROR: BUFFER FULL\n");
  	    } 
  	    else
    	    buffer[bufferPointer++] = c;
	}	
}
	//*4-8*//
#define TRUE 1
#define FALSE 0
int pushedBack;
int havePushedBack = FALSE;


int modifiedGetch(void){
	int c =  (havePushedBack) ? pushedBack : getchar();  
	if(havePushedBack)
		havePushedBack = FALSE;
	return c;
}	

void modifiedUngetch(int c){
	if(havePushedBack)
		printf("A character is already pushed back\n");
	
	else{
		havePushedBack = TRUE;
		pushedBack = c;
	}

}