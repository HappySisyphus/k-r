#include <ctype.h> // For isdigit()
#include <stdio.h> // EOF
#include <string.h>
#include "calculator.h"
#define MAX_INPUT 1024


char input[MAX_INPUT];
int inputIndex = 0;
int hasNewInput = TRUE;
int c;
int length = 0;
//for 4-11
//static int pushedBack;
//static int hasPushedBack = FALSE;

// 4-10
int getop1(char s[]) {

  if (inputIndex == length) {
    hasNewInput = TRUE;
  }

  int i;
  if (hasNewInput) {
    length = getline1(input, MAX_INPUT);
    hasNewInput = FALSE;
    inputIndex = 0;
  }

  while (input[inputIndex] == ' ' || input[inputIndex] == '\t')
    inputIndex++;

  s[0] = input[inputIndex];
  s[1] = '\0';

  if (!isdigit(input[inputIndex]) && input[inputIndex] != '.') {
    if (input[inputIndex] >= 'a' && input[inputIndex] <= 'z') {
      inputIndex++;
      return VARIABLE;
    }
    return input[inputIndex++];
  }

  i = 0;
  if (isdigit(input[inputIndex])) {
    while (isdigit(s[++i] = input[++inputIndex]))
      ;
  }
  if (input[inputIndex] == '.')
    while (isdigit(s[++i] = input[++inputIndex]))
      ;

  s[i] = '\0';

  return NUMBER;
}

int getop(char s[]) {

  int i, c;
  //4-11 static varibles are guaranteed to be initilized to 0
  static int pushedBack;
  static int hasPushedBack;
 
 if(hasPushedBack){
    c = pushedBack;
    hasPushedBack = FALSE;
  }
  else 
      c = getch();

  while ((s[0] = c) == ' ' || c == '\t')
      c = getch();
  // Reduntant?
  s[1] = '\0';

  if (!isdigit(c) && c != '.') {
    if (c >= 'a' && c <= 'z') {
      return VARIABLE;
    }
    return c; // Is not a number
  }
  i = 0;
  if (isdigit(c))
    while (isdigit(s[++i] = c = getch()))
      ; // collects the integer part of the number
  ;
  if (c == '.')
    while (isdigit(s[++i] = c = getch()))
      ;
  s[i] = '\0';
  if (c != EOF)
    //4-11
    pushedBack = c;
    hasPushedBack = TRUE;//ungetch(c); // Push back c to the input
  return NUMBER;
}

// 4-7
void ungets(char s[]) {
  printf("%s\n", s);
  int length = strlen(s);
  for (int i = length - 1; i >= 0; i--)
    ungetch(s[i]);
}

int getline1(char line[], int lim) {
  int character;
  int stringLength = 0;
  while (--lim > 0 && (character = getchar()) != EOF && character != '\n') {
    line[stringLength++] = character;
  }
  if (character == '\n') {
    line[stringLength++] = character;
  }
  line[stringLength] = '\0';

  return (stringLength);
}
