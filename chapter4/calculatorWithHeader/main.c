#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "calculator.h"
#define MAXOP 100
#define MAX_NO_VARIBLES 26


int main(void) {

  double latestValue;
  int operand2;
  int type;
  char number[MAXOP];
  double variables[MAX_NO_VARIBLES];
  double isAssigned[MAX_NO_VARIBLES];
  int variableIndex;
  int assign;



  while ((type = getop1(number)) != EOF) {

    switch (type) {

    case 'V':
      assign = TRUE;
      break;
    // For 4-6
    // A varible can only be assigned to the last calculated value
    // This is done by inputing V character.
    // example input V b would assign the latest value to b.
    case 'a':
      printf("%c\n", number[0]);
      variableIndex = (number[0] - 'a') % (MAX_NO_VARIBLES - 1);
      if (assign) {
        variables[variableIndex] = latestValue;
        isAssigned[variableIndex] = TRUE;
        assign = FALSE;
      }

      else if (isAssigned[variableIndex]) {
        push(variables[variableIndex]);
      } else
        printf("Varible %c is not assigned\n", number[0]);
      break;

    case '0':
      push(atof(number));
      break;

    case '+':
      push(pop() + pop());
      break;
    case '*':
      push(pop() * pop());
      break;
    case '-':
      operand2 = pop();
      push(pop() - operand2);
      break;
    case '/':
      if ((operand2 = pop()) != 0.0)
        push(pop() / operand2);
      else
        printf("DIVISION BY ZERO\n");
      break;
    // For 4-3
    case '%':
      if ((operand2 = pop()) != 0.0)
        push((int)pop() % (int)operand2);
      else
        printf("Modulo with 0 is not defined");
      break;
    // For 4-4
    case 'C':
      printf("Clearing Stack\n");
      clear();
      break;
    case 'D':
      printf("Dulplicate the top elements\n");
      duplicate();
      break;
    case 'T':
      printf("%f\n", topElement());
      break;
    case 'S':
      printf("Swapping top Elements\n");
      swapTopElements();
      break;
    // For 4-5
    case '!':
      push(sin(pop()));
      break;
    case '"':
      push(cos(pop()));
      break;
    case '$':
      push(exp(pop()));
      break;
    case '&':
      operand2 = pop();
      push(pow(pop(), operand2));
      break;
    case '\n':
      if (isEmpty())
        latestValue = pop();
      printf(" Latest Value\t%.8g\n", latestValue);
      break;
    default:
      printf("UNKOWN COMMAND\n");
      break;
    }
  }
  return 0;
}
