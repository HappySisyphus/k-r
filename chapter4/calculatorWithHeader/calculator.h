#define NUMBER '0'
#define VARIABLE 'a'
#define TRUE 1
#define FALSE 0
//Function declarations
int getop(char[]);
int getop1(char[]);
void push(double);
double pop(void);
double topElement(void);
int isEmpty(void);
void swapTopElements(void);
void duplicate(void);
void clear(void);
int getch(void);
void ungetch(int);
void ungets(char s[]);
int getline1(char line[], int lim);