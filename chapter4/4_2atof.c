#include <ctype.h>
#include <stdio.h>
#define TRUE 1
#define FALSE 0
double atofModified(char string[]);

int main(void) {
  printf("%1.9f\n", atofModified("1e-1"));
  printf("%1.9f\n", atofModified("1e-2"));
  printf("%1.9f\n", atofModified("123e2"));
  return 0;
}

double atofModified(char string[]) {
  double val, power,multiplierExponent, exponentSign;
  int i, sign, scienceNotation, exponent;
  scienceNotation = FALSE;
  multiplierExponent = 1.0;

  for (i = 0; isspace(string[i]); i++)
    ;
  sign = (string[i] == '-') ? -1 : 1;
  if (string[i] == '+' || string[i] == '-') {
    i++;
  }
  for (val = 0.0; isdigit(string[i]); i++) {
    val = 10.0 * val + (string[i] - '0');
  }
  if (string[i] == '.')
    i++;
  for (power = 1.0; isdigit(string[i]); i++) {
    val = 10.0 * val + (string[i] - '0');
    power *= 10.0;
  }

  // Check if the string is given in scientific notation
  if (string[i] == 'e' || string[i] == 'E') {
    i++;
    scienceNotation = TRUE;
  }

  if (scienceNotation) {

    // Taking care of the scientific notation
    exponentSign = (string[i] == '-') ? 0.1 : 10.0;
    if (string[i] == '+' || string[i] == '-')
      i++;
    // Calculates to what power the exponent is raised
    for (exponent = 0; isdigit(string[i]); i++) {
      exponent = 10 * exponent + (string[i] - '0');
    }
    // Calculates the exponent that is to be multiplied
    // with the prefix number.
    while (exponent > 0) {
      multiplierExponent *= exponentSign;
      exponent--;
    }
  }
  return (sign * val / power) * multiplierExponent;
}
