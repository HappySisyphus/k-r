#include <stdio.h>
#include <string.h>
void reverse(char s[]);
int main(void) {
  char string[10] = "SIMON :)";
  char string1[10] = "PIPOLO";
  reverse(string);
  reverse(string1);
  printf("%s\n", string1);
}

// Dont really like how this functions looks like
// The problem does not really have a recursive nature
// So solvining it recursivly is a bit wierd.
void reverse(char s[]) {
  static int i = 0;
  int j = strlen(s) - 1 - i;

  if (i < j) {
    char temp = s[i];
    s[i] = s[j];
    s[j] = temp;
    i++;
    reverse(s);
  }

  i = 0;
  j = 0;
}