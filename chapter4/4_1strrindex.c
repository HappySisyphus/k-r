#include <stdio.h>
#include<stdlib.h>
#define TRUE 1
#define FALSE 0
#define MAXLENGTH 1024

int strrindex(char line[], char pattern[]);
int getline1(char line[], int lim);
int main(void){
	int k;
	char line[MAXLENGTH];
	char pattern[MAXLENGTH] = "FOO";
	while(getline1(line, MAXLENGTH)>0){
		if((k = strrindex(line,pattern))>=0){
			printf("%d\n",k);
		}
	}
	return 0;
}




int getline1(char line[], int lim){
	int character;
	int stringLength = 0;
	while(--lim>0 &&(character = getchar())!=EOF && character !='\n'){
		line[stringLength++] = character;
	}
	if(character == '\n'){
		line[stringLength++] = character;
	}
	line[stringLength] = '\0';

	return (stringLength);
}

int strrindex(char line[], char pattern[]){
	int i,j,k;
	int index = -1;
	for(i = 0; line[i]!='\0'; i++){
		for(j=i, k=0; pattern[k]!='\0' && pattern[k]==line[j]; k++, j++){
				asm("NOP");
		}
		if(k>0 && pattern[k]=='\0')
				index = i;
	}
	return index;
}