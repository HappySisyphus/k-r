#include <stdio.h>
#define MAXLINE 1000

int getline1(char line[], int max);
int strindex(char source[], char searchFor[]);
int main(void){
	int k;
	char line[MAXLINE];
	char pattern[MAXLINE] = "FOO";
	while(getline1(line,MAXLINE)>0){
		if((k = strindex(line,pattern))>=0){
			printf("%d\n",k);
		}
	}
	return 0;}



// Stores the input string in line and returns the index to the last character of the string
int getline1(char line[], int lim){
	int character;
	int stringLength = 0;
	while(--lim>0 &&(character = getchar())!=EOF && character !='\n'){
		line[stringLength++] = character;
	}
	if(character == '\n'){
		line[stringLength++] = character;
	}
	line[stringLength] = '\0';

	return (stringLength);
}

int strindex(char source[], char searchFor[]){
	int i,j,k;
	int index = -1;

	for(i = 0; source[i]!='\0'; i++){
		for(j=i, k=0; searchFor[k]!='\0'&& searchFor[k]==source[j]; j++, k++)
			;
	
		if(k > 0 && searchFor[k] == '\0')
			index = i;
	}
	if(index == -1)
		return -1;

	return index;
}

