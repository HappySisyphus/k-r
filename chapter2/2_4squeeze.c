/*
* @Author: simonvonschmalensee
* @Date:   2018-06-28 18:12:34
* @Last Modified by:   simonvonschmalensee
* @Last Modified time: 2018-06-28 18:19:48
*/
#include <stdio.h>
#include <stdlib.h>

void squeeze(char string1[], char string2[]);

int main(void){
char a[10] = "abcdefj";
char b [10] = "dajf";
	squeeze(a,b);
	printf("%s\n", a);
	return 0;
}


void squeeze(char string1[], char string2[]){
	int i;
	for(i = 0; string1[i] != '\0'; i++){	
		for(int k = 0; string2[k]!='\0'; k++){
			if(string1[i] == string2[k]){
				for(int l = i; string1[l]!='\0'; l++){
					string1[l] = string1[l+1];
				}
				k = -1;
			}
		}
	}
	string1[i] = '\0';
}

