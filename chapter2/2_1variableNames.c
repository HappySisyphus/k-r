/*
* @Author: simonvonschmalensee
* @Date:   2018-06-28 17:59:57
* @Last Modified by:   simonvonschmalensee
* @Last Modified time: 2018-06-28 18:28:45
*/
#include <stdio.h>
#include <limits.h>

#define MAX_UNSIGNED_INT 0XFFFFFFFF
#define MAX_SIGNED_INT 0X0FFFFFFF // 2's complement

int main(void)
{ 
	printf("%u\n",MAX_UNSIGNED_INT); 
	printf("%u\n", UINT_MAX);
	printf("%d\n", MAX_SIGNED_INT);
	printf("%d\n", INT_MAX);



}


