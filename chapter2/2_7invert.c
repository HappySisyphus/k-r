#include <stdio.h>

unsigned int invert(unsigned int x, int p, int n);

int main(void){
	printf("%u\n",invert(127,0,7));


	return 0;
} 

unsigned int invert(unsigned int x, int p, int n){

	//Mask out the n bits begining at position p
	//set the n bits begining at position p in x to 0
	// invert the masked out bits and or with x
	int k = x & (((1<<n)-1)<<p);
	x =  x & ~(((1<<n)-1)<<p);
	x = x | (k^(((1<<n)-1)<<p));

	return x;
}
