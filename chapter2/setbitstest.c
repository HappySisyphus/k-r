#include <stdio.h>
#include <stdlib.h>

unsigned int setbits(unsigned int x, int p, int n, unsigned int y);
unsigned int setbits2(unsigned int x, int p, int n, unsigned int y);
unsigned setbits3(unsigned x,int p,int n,unsigned y);
unsigned int setbits4(unsigned int x, int p, int n, unsigned int y);
int main(void)
{
    //printf("%u\n", setbits(170, 4, 3, 7));
    printf("%u\n", setbits2(250, 4, 23, 7));
     //printf("%u\n", setbits3(170, 4, 3, 7));
      printf("%u\n", setbits4(250, 4, 23, 7));
    return EXIT_SUCCESS;
}

/* getbits:  place n rightmost bits from position p at position y */
unsigned int setbits(unsigned x, int p, int n, unsigned int y)
{
    int i, mask, j;
    /* The rightmost n bits of y. */
    i = (x >> (y+1-n)) & ~(~0 << n);
    /* A mask with zeros at the rightmost n bits of p. */
    mask = ~(((1 << n)-1) << (p+1-n));
    /* Make rightmost n bits of p zero ... */
    j = mask & x;
    /* ... and replace them with the rightmost n bits of y. */
    return j | i << (p+1-n);
}

unsigned int setbits2(unsigned int x, int p, int n, unsigned int y) {
  return (x & ((~0 << (p + 1)) | (~(~0 << (p + 1 - n))))) |
         ((y & ~(~0 << n)) << (p + 1 - n));
}

unsigned setbits3(unsigned x,int p,int n,unsigned y)
{
    return x & ~(~(~0 << n) << (p+1-n)) | ( y & ~(~0<<n)) << (p+1-n);
}


unsigned int setbits4(unsigned int x, int p, int n, unsigned int y) {
  return (x & ~(((1 << n) - 1) << (p-1))) | ((y & ((1 << n) - 1)) << (p-1));}




