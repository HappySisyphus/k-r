/*
* @Author: simonvonschmalensee
* @Date:   2018-06-28 11:08:02
* @Last Modified by:   simonvonschmalensee
* @Last Modified time: 2018-06-28 18:28:44
*/
//for (i = 0; i<lim-1 && (c=getchar())!='\n' && c!=EOF; i++)
#include <stdio.h>
#define MAX_LENGTH 1000

int getline2(char line[], int maxInputLength);
int getline2WithoutMultipleReturns(char line[], int maxInputLength);
char c;
int length;
int main (void){
	char line[MAX_LENGTH];
	while((length = getline2WithoutMultipleReturns(line, MAX_LENGTH))>0){
		printf("%d\n", length);
		printf("%s\n", line);
	}

}




int getline2(char line[], int maxInputLength){
	
	for(int i = 0; i<maxInputLength-1; i++){
		if((c=getchar())=='\n'){
			line[i] = c;
			return i;
		}
		if(c==EOF){
			return i;
		}
	line[i] = c;
	}
 return 0;
}

int getline2WithoutMultipleReturns(char line[], int maxInputLength){
	int character;
	int i = 0;
	

	while(i<maxInputLength-1){
		character = getchar();
		if(character == '\n'){
			line[i] = character;
			//i++;
			//Ok since we pass by value
			maxInputLength = 0;
		}
		else if(character == EOF)
		{	line[i] = '\0';
			maxInputLength = 0;
			i = 0;}

		else{
		line[i] = character;
		i++;}
	
	}
	line[i+1] = '\0';
	return i;
}








