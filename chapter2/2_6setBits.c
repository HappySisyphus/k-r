#include <stdio.h>

unsigned int setbits(unsigned int x, int p, int n, unsigned int y);
int main(void) { 
	printf("%u\n", setbits(250, 3, 14, 127)); 
}

unsigned int setbits(unsigned int x, int p, int n, unsigned int y) {
  return (x & ~(((1 << n) - 1) << (p))) | ((y & ((1 << n) - 1)) << (p));
}
