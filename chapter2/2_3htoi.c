/*
* @Author: simonvonschmalensee
* @Date:   2018-06-28 12:34:10
* @Last Modified by:   simonvonschmalensee
* @Last Modified time: 2018-06-28 18:28:43
*/
#include <stdio.h>
#include <stdlib.h>
const char buffer[100] = "000F00000"; 
int htoi(const char hexValue[]);

int main(void){
	printf("%d\n",htoi(buffer));
}





int htoi(const char hexValue[]){
	int convertedValue = 0;
	int character;
	int i = 0; 
	
	//If the hex-value is formated as 0X..... or 0x..... 
	if(hexValue[0] == 0 && (hexValue[1] == ('x') || hexValue[1] =='X')){
		i = 2;
	}


	while(hexValue[i]!='\0'){
		int decValue;
		character = hexValue[i];
	//To lower
	if(character >= 'A' && character<='Z'){
		character = character + 'a' - 'A';
	}
	//Is a letter(a-f)
	if(character >= 'a' && character<='f'){
		decValue = 10 + character - 'a';
	}
	//Is a decimal value
	else{
		decValue = character - '0';
	}
 	 convertedValue = 16*convertedValue + decValue;
 	 i++;
 	}
 	return convertedValue; 
 }



	





