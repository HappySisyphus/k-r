/*
* @Author: simonvonschmalensee
* @Date:   2018-07-03 09:45:12
* @Last Modified by:   simonvonschmalensee
* @Last Modified time: 2018-07-03 09:48:12
*/
#include <stdio.h>

int bitcountSigned(int x);
int main(void){
	printf("%d\n",bitcountSigned(-127));
	return 0;
}

int bitcountSigned(int x){
	int numberOfOnes;
	for(numberOfOnes=0; x!=0; x &= (x-1)){
		numberOfOnes++;
	} 
	return numberOfOnes; 
}

