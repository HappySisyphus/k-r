#include <stdio.h>

char toLower(char c);
int main(void){
	printf("%c\n",toLower('K'));
}

char toLower(char c){
	return (c <='Z' && c >= 'A') ? (c+32) : c;
}

