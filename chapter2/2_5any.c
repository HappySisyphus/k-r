/*
* @Author: simonvonschmalensee
* @Date:   2018-06-28 18:26:56
* @Last Modified by:   simonvonschmalensee
* @Last Modified time: 2018-06-28 18:27:32
*/
#include <stdio.h>
#include <stdlib.h>




int any(char string1[], char string2[]);
int main(void){
	char a[10] = "ABCD";
	char b[10] = "JDBC";
	printf("%d\n", any(a,b));
	printf("%c\n",a[any(a,b)]);


	return 0;}



int any(char string1[], char string2[]){
	int i;
	for(int i = 0; string1[i] != '\0'; i++){
		for(int k = 0; string2[k]!='\0'; k++){
			if(string1[i] == string2[k]){
				return i;
			}
		}
	}
return -1;
} 

