int getch(void);
void ungetch(int c);
 int getword1(char *word, int lim) {

  int character;
  char *w = word;
  static int isString = 0;
  int isPreProcessor = 0;
  while (isspace(character = getch()))
    ;

  if (character == '"') {
    while ((character = getch()) != '"')
      ;
  }

  if (character == '/') {
    char temp = getch();
    if (temp == '*') {
      while (getch() != '*' && getch() != '/')
        ;
    } else if (temp == '\n')
      ;
    else
      while (getch() != '\n')
        ;
  }
  if (character == '#' || character == '_') {

    while (getch() != '\n')
      ;
  }

  if (character != EOF)
    *w++ = character;

  if (!isalpha(character)) {
    *w = '\0';
    return character;
  }

  for (; --lim > 0; w++) {
    *w = getch();
    if (!isalnum(*w) || *w == '_') {
      ungetch(*w);

      break;
    }
  }
  if (*w == '_') {
    return '\0';
  }
  *w = '\0';
  return word[0];
}


#define BUFFER_SIZE 100

int pushbackBuffer[BUFFER_SIZE];
int bufferPointer = 0;
int getch(void){

	return (bufferPointer > 0) ? pushbackBuffer[--bufferPointer] : getchar();
}

void ungetch(int character){

	if(bufferPointer!= BUFFER_SIZE-1){
		pushbackBuffer[bufferPointer++] = character;
	}
	else 
		printf("Error, buffer full");

}