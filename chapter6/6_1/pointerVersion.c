#include <ctype.h>
#include <stdio.h>
#include <string.h>

#define MAX_WORD 100

struct key {
  char *word;
  int count;
};

struct key keytab[] = {
    {"auto", 0},     {"break", 0},    {"case", 0}, {"char", 0},   {"const", 0},
    {"continue", 0}, {"default", 0},  {"int", 0},  {"static", 0}, {"return", 0},
    {"void", 0},     {"volatile", 0}, {"while", 0}};

#define NUMBER_OF_KEYS (sizeof keytab / sizeof(struct key))
int getword1(char *, int);
int binsearch1(char *, struct key *, int);

int main(int argc, char *argv[]) {
        
  int n;
  char word[MAX_WORD];

  while (getword1(word, MAX_WORD) != EOF) {
    if (isalpha(word[0]))
      if ((n = binsearch1(word, keytab, NUMBER_OF_KEYS)) >= 0)
        keytab[n].count++;
  }
  for (n = 0; n < NUMBER_OF_KEYS; n++) {
    printf("%4d %s\n", keytab[n].count, keytab[n].word);
  }
  return 0;
}

int binsearch1(char *word, struct key tab[], int n) {
  int cond;
  int low, high, mid;
  low = 0;
  high = n - 1;
  while (low <= high) {
    mid = (low + high) / 2;
    if ((cond = strcmp(word, tab[mid].word)) < 0)
      high = mid - 1;
    else if (cond > 0)
      low = mid + 1;
    else
      return mid;
  }
  return -1;
}

int getch(void);
void ungetch(int c);
int getword1(char *word, int lim) {

  int character;
  char *w = word;
  static int isString = 0;
  int isPreProcessor = 0;
  while (isspace(character = getch()))
    ;

  if (character == '"') {
    while ((character = getch()) != '"')
      ;
  }

  if (character == '/') {
    char temp = getch();
    if (temp == '*') {
      while (getch() != '*' && getch() != '/')
        ;
    } else if (temp == '\n')
      ;
    else
      while (getch() != '\n')
        ;
  }
  if (character == '#' || character == '_') {

    while (getch() != '\n')
      ;
  }

  if (character != EOF)
    *w++ = character;

  if (!isalpha(character)) {
    *w = '\0';
    return character;
  }

  for (; --lim > 0; w++) {
    *w = getch();
    if (!isalnum(*w) || *w == '_') {
      ungetch(*w);

      break;
    }
  }
  if (*w == '_') {
    return '\0';
  }
  *w = '\0';
  return word[0];
}

#define BUFFER_SIZE 100

int pushbackBuffer[BUFFER_SIZE];
int bufferPointer = 0;
int getch(void) {

  return (bufferPointer > 0) ? pushbackBuffer[--bufferPointer] : getchar();
}

void ungetch(int character) {

  if (bufferPointer != BUFFER_SIZE - 1) {
    pushbackBuffer[bufferPointer++] = character;
  } else
    printf("Error, buffer full");
}