#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define MAX_WORD_LENGTH 100

struct treeNode *addToTree(struct treeNode *p, char *w, int prefixLength);
struct treeNode *talloc(void);
void printTree(struct treeNode *p);
char *strdup1(char *s,int prefixLength);
int getword1(char *word, int lim);
int isType(char *word);
int binsearch1(char *word);