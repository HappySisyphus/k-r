/*The program assumes that the c-program is written in a correct manor, also, */



#include "tree.h" 
#define SIZE_OF_LOOK_UP 6
#define TRUE 1
#define FALSE 0
static char *varibleLookUpTable[SIZE_OF_LOOK_UP] =
{	"char",
	"double",
	"enum",
	"int",
	"long",
	"short"
};
volatile int isInGroup = 0;
volatile int isVarible = 0;

struct treeNode {
  char *word;
  int inGroup;
  struct treeNode *left;
  struct treeNode *right;
};

int main(int argc, char *argv[]) {

  int prefixLength;
  if (argc > 1) {
    prefixLength = atoi(*(argv + 1));
    if (prefixLength < 0){
     	 printf("Prefix length must be positive\n");
	}
  } 
	else{
    prefixLength = 6;}

  struct treeNode *root;
  char word[MAX_WORD_LENGTH];

  root = NULL;
  while (getword1(word, MAX_WORD_LENGTH) != EOF) {
    if ((isalpha(word[0]) ||  word[0] == '_') && !isType(word))
		root = addToTree(root, word, prefixLength);
  }
  printTree(root);
  return 0;
}


int isType(char *word){
	return binsearch1(word);
}

int binsearch1(char *word) {
  int cond;
  int low, high, mid;
  low = 0;
  high = SIZE_OF_LOOK_UP - 1;
  while (low <= high) {
    mid = (low + high) / 2;
    if ((cond = strcmp(word, varibleLookUpTable[mid])) < 0)
      high = mid - 1;
    else if (cond > 0)
      low = mid + 1;
    else
       return 1;
  }
  return 0;
}


struct treeNode *addToTree(struct treeNode *p, char *w, int prefixLength) {

  int condition;

  if (p == NULL) {
    p = talloc();
    p->word = strdup1(w, prefixLength);
    p->inGroup = isInGroup;
    isInGroup = 0; 
    p->left = p->right = NULL;
  }
  else{
  	condition = strncmp(w, p->word, prefixLength);
  	printf("%d\n",condition);
  	if (condition == 0) {
  		printf("FOO\n");
    	p->inGroup = 1;
    	isInGroup = 1;

  } else if (condition < 0)
    	p->left = addToTree(p->left, w, prefixLength);
  	else if (condition > 0)
    	p->right = addToTree(p->right, w, prefixLength);
}
  return p;
}

void printTree(struct treeNode *p) {
  if (p != NULL) {
    printTree(p->left);
    if(p->inGroup == 1){
    printf("%s\n", p->word);}
    printTree(p->right);
  }
}

struct treeNode *talloc(void) {
  return malloc(sizeof(struct treeNode));
}

char *strdup1(char *s, int prefixLength) {
  char *p;

  p = (char *)malloc(prefixLength + 1);
  if (p != NULL) {
    strncpy(p, s, prefixLength);
  }
  return p;
}

int getch(void);
void ungetch(int c);
int getword1(char *word, int lim) {

  int character;
  char *w = word;
  while (isspace(character = getch()))
    ;

  if (character == '"') {
    while ((character = getch()) != '"')
      ;
  }

  if (character == '/') {
    char temp = getch();
    if (temp == '*') {
      while (getch() != '*' && getch() != '/')
        ;
    } else if (temp == '\n')
      ;
    else
      while (getch() != '\n')
        ;
  }
  if (character == '#') {

    while (getch() != '\n')
      ;
  }

  if (character != EOF)
    *w++ = character;

  if (!isalpha(character)) {
    *w = '\0';
    return character;
  }

  for (; --lim > 0; w++) {
    *w = getch();
    if (!isalnum(*w)) {
      ungetch(*w);

      break;
    }
  }
  if (*w == '_') {
    return '\0';
  }
  *w = '\0';
  return word[0];
}

#define BUFFER_SIZE 100

int pushbackBuffer[BUFFER_SIZE];
int bufferPointer = 0;

int getch(void) {
  return (bufferPointer > 0) ? pushbackBuffer[--bufferPointer] : getchar();
}

void ungetch(int character) {

  if (bufferPointer != BUFFER_SIZE - 1) {
    pushbackBuffer[bufferPointer++] = character;
  } else
    printf("Error, buffer full");
}
