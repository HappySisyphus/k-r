#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#define MAX_WORD_LENGTH 100




struct treeNode{
	char *word;
	int count;
	int read;
	struct treeNode *left;
	struct treeNode *right;
};

struct treeNode *addToTree(struct treeNode *p, char *word);
struct treeNode *getItem(struct treeNode *p);
void printTree(struct treeNode *p);
int getword(char *word, int maxWordLength);
struct treeNode* traverseAndAdd(struct treeNode *p);
struct treeNode* addToTreeSortedByCount(struct treeNode *p,struct treeNode *n);
