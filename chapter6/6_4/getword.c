/*
* @Author: simonvonschmalensee
* @Date:   2018-07-25 09:21:07
* @Last Modified by:   simonvonschmalensee
* @Last Modified time: 2018-07-25 11:45:51
*/
#define BUFFER_SIZE 100
#include "main.h"
int pushbackBuffer[BUFFER_SIZE];
int *bufferPointer = pushbackBuffer;


int getch(void);
void ungetch(int);


int getword(char *word, int maxWordLength)
{
	int character;
	char *wordPointer = word;

	while(isspace(character = getch()))
			;

	if (character != EOF)
		*wordPointer++ = character;
	if(!isalpha(character)){
		*wordPointer = '\0';
		return character;
	}

	for (; --maxWordLength > 0; wordPointer++){
		*wordPointer = getch();
		if(!isalnum(*wordPointer) || *wordPointer == ' ' ){
			ungetch(*wordPointer);
			break;
		}
	}
	*wordPointer = '\0';
	return word[0];
}



int getch(void){
	return (bufferPointer-&pushbackBuffer[0]) ?  *(--bufferPointer) : getchar();
}

void ungetch(int character){
	if((bufferPointer-&pushbackBuffer[0])!=BUFFER_SIZE){
		*bufferPointer++ = character;
	}
	else
		printf("ERROR: buffer is full");
}








