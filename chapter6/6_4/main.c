/*
* @Author: simonvonschmalensee
* @Date:   2018-07-24 08:11:37
* @Last Modified by:   simonvonschmalensee
* @Last Modified time: 2018-07-25 12:40:58
05
*/
#include "main.h"


struct treeNode *root;
struct treeNode *sortedByCountroot;
struct treeNode *temporaryItem;
int main(int argc, char* argv[]){

	root = NULL;
	temporaryItem = NULL;
	sortedByCountroot =NULL;

	char word[MAX_WORD_LENGTH];
	
	
	while(getword(word, MAX_WORD_LENGTH)!=EOF){
		if(isalpha(word[0])){
			root = addToTree(root,word);}
	}
	
	while((temporaryItem = getItem(root)) != NULL)
	{
		sortedByCountroot = addToTreeSortedByCount(sortedByCountroot,
													temporaryItem);
	}
	
	printTree(sortedByCountroot);
	//printTree(root);
	return 0;
}



