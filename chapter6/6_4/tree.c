/*
* @Author: simonvonschmalensee
* @Date:   2018-07-24 08:12:56
* @Last Modified by:   simonvonschmalensee
* @Last Modified time: 2018-07-25 13:22:36
*/
#include "main.h"
struct treeNode *talloc(void);
char *strdup1(char *);
struct treeNode* addToTreeSortedByCount(struct treeNode *p, struct treeNode *n);


struct treeNode *getItem(struct treeNode *p){
	if(p->left != NULL && !p->left->read){
		return getItem(p->left);
	}
	else if(p->right != NULL && !p->right->read){
		return getItem(p->right);
	}
	else if(p->read == 0){
		p->read = 1;
		return p;
	}
	else 
		return NULL;
}

struct treeNode* addToTreeSortedByCount(struct treeNode *p,struct treeNode *n){


	
	if(p == NULL){
		
		printf("%s",n->word);
		p = talloc();
		p->word = strdup1(n->word);
		p->count = n->count;
		p->left = p->right = NULL;
	}

	
	else if (n->count <= p->count){
		
		p->left = addToTreeSortedByCount(p->left, n);
	}
	else if(n->count > p->count){
		p->right = addToTreeSortedByCount(p->right, n);
	}

	return p;
}


struct treeNode *addToTree(struct treeNode *p,char *word){

	
	int cond;
	if(p == NULL){

		p = talloc();
		p->word = strdup1(word);
		p->count = 1;
		p->read = 0;
		p->left = p->right = NULL;
	}
	else if((cond = (strcmp(word, p->word))) == 0){
		p->count++;
	}
	else if (cond < 0)
		p->left = addToTree(p->left, word);
	else if(cond > 0)
		p->right = addToTree(p->right, word);

	return p;
}

void printTree(struct treeNode *p){
	if(p!=NULL){
	printTree(p->right);
	printf("%s number of occurences %d\n",p->word, p->count);
	printTree(p->left);
	}
}


struct treeNode *talloc(void){
	return (struct treeNode *) malloc(sizeof(struct treeNode));
}

char *strdup1(char *s){
	char *p;
	p = (char *)malloc(strlen(s)+1);
	if(p!= NULL){
		strcpy(p,s);
	}
	return p;
}





