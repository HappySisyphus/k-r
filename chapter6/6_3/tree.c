/*
* @Author: simonvonschmalensee
* @Date:   2018-07-24 08:12:56
* @Last Modified by:   simonvonschmalensee
* @Last Modified time: 2018-07-25 09:19:59
*/
#include "main.h"x
struct treeNode *talloc(void);
char *strdup1(char *);

extern struct treeNode *root;

struct treeNode* addToTree(struct treeNode *p, char *word, int lineNumber){

	int cond;

	if(p == NULL){

		p = talloc();
		p->word = strdup1(word);
		p->count = 1;
		p->lineNumber[0] = lineNumber;
		p->left = p->right = NULL;
	}
	else if((cond = strcmp(word, p->word)) == 0){
		if(lineNumber != p->lineNumber[p->count])
			p->lineNumber[p->count++] = lineNumber;
	}
	else if (cond < 0)
		p->left = addToTree(p->left, word,lineNumber);
	else if(cond > 0)
		p->right = addToTree(p->right, word,lineNumber);

	return p;
}

void printTree(struct treeNode *p){
	if(p!=NULL){
	printTree(p->left);
	printf("Word: %s Linenumber'(s) :",p->word);
	for(int i = 0; i<p->count; i++)
		printf("%d, ", p->lineNumber[i]);
	printf("\n");
	printTree(p->right);
	removeNode(p);
	}
}

struct treeNode *talloc(void){
	return (struct treeNode *) malloc(sizeof(struct treeNode));
}

char *strdup1(char *s){
	char *p;
	p = (char *)malloc(strlen(s)+1);
	if(p!= NULL){
		strcpy(p,s);
	}
	return p;
}

void removeNode(struct treeNode *toBeRemoved){
	if (toBeRemoved == NULL){
		return;
	}
	
	printf("REMOVED %s \n",toBeRemoved->word);
	free(toBeRemoved->word);
	free(toBeRemoved);

	
	

}
