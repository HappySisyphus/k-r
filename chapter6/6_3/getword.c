#define BUFFER_SIZE 100
#include "main.h"
int pushbackBuffer[BUFFER_SIZE];
int *bufferPointer = pushbackBuffer;


int getch(void);
void ungetch(int);


int getword(char *word, int maxWordLength)
{
	int character;
	char *wordPointer = word;

	if((character = getch()) == '\n'){
		*wordPointer =  character;
		return word[0];
	}
	while(isspace(character))
		character = getch();

	if (character != EOF)
		*wordPointer++ = character;
	if(!isalpha(character)){
		*wordPointer = '\0';
		return character;
	}

	for (; --maxWordLength > 0; wordPointer++){
		if(!isalnum(*wordPointer = getch())){
			ungetch(*wordPointer);
			break;
		}
	}
	*wordPointer = '\0';
	return word[0];
}



int getch(void){
	return (bufferPointer-&pushbackBuffer[0]) ?  *(--bufferPointer) : getchar();
}

void ungetch(int character){
	if((bufferPointer-&pushbackBuffer[0])!=BUFFER_SIZE){
		*bufferPointer++ = character;
	}
	else
		printf("ERROR: buffer is full");
}








