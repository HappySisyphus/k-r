#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#define MAX_LINE_NUMBER 100
#define MAX_WORD_LENGTH 100




struct treeNode{
	char *word;
	int count;
	int lineNumber[MAX_LINE_NUMBER];
	struct treeNode *left;
	struct treeNode *right;
	struct treeNode *parent;
};

struct treeNode *addToTree(struct treeNode *p, char *word, int lineNumber);
void removeNode(struct treeNode *toBeRemoved);
void printTree(struct treeNode *p);
int getword(char *word, int maxWordLength);
int isCommonWord(char *word);
