/*
* @Author: simonvonschmalensee
* @Date:   2018-07-24 08:11:37
* @Last Modified by:   simonvonschmalensee
* @Last Modified time: 2018-07-24 17:11:40
05
*/
#include "main.h"

char *commonWords[] = {
	"and",
	"if",
	"or",
	"that",
	"the",
	NULL


};

struct treeNode *root;


int lineNumber = 1; // First line
int main(int argc, char* argv[]){
root = NULL;
	char word[MAX_WORD_LENGTH];


	while(getword(word,MAX_WORD_LENGTH) != EOF){
		if(word[0] == '\n'){
			lineNumber++;
		}
		else if(isalpha(word[0]) && !isCommonWord(word))
			root = addToTree(root,word,lineNumber);
	}
	printTree(root);
	//printTree(root);
	return 0;
}

int isCommonWord(char *word){
	char **pointerToIllegalWords = commonWords;
	while(*pointerToIllegalWords!=NULL){
		if(strcmp(*pointerToIllegalWords,word) == 0){
			return 1;
		}
		pointerToIllegalWords++;
	}

	return 0;
}


