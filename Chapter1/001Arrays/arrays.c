#include <stdio.h>

int main(void){
    
    int character, noOfWhiteSpace, noOfOther;
    int noOfDigits[10];

    noOfWhiteSpace = noOfOther = 0;
    /*Fill the array with zeros*/
    for (int i = 0; i<10; i++){
    	noOfDigits[i] = 0;
    }

    while((character = getchar()) != EOF){
    	if (character >='0' && character <='9'){
    		++noOfDigits[character-'0'];
    	}
    	else if (character == ' '||character == '\n'||character == '\t'){
    		noOfWhiteSpace++;
    	} 
    	else{
    		noOfOther++;
    	}
    }
    printf("Digits\n");
    for(int i = 0; i<10; i++){
    	printf("%d ",noOfDigits[i]);
    }
    printf("White space = %d, Other = %d\n", noOfWhiteSpace, noOfOther);





}