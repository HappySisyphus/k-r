/*
* @Author: simonvonschmalensee
* @Date:   2018-06-24 17:42:39
* @Last Modified by:   simonvonschmalensee
* @Last Modified time: 2018-06-24 17:57:18
*/

#include <stdio.h>
#define NO_OF_ASCII_CHARS 128
int main(void){

	int character;
	int histogram[NO_OF_ASCII_CHARS];

	for(int i = 0; i<NO_OF_ASCII_CHARS; i++){
		histogram[i] = 0;
	}

	while( (character=getchar()) != EOF){
		++histogram[character];
	}

	for(int i = 0; i<NO_OF_ASCII_CHARS; i++){ 
		printf("%d", i);
		for (int j = 0; j<histogram[i]; j++){
			printf("x");
		}
		printf("\n");
	}

}