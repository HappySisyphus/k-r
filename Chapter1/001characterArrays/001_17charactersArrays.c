/*
* @Author: simonvonschmalensee
* @Date:   2018-06-25 20:47:21
* @Last Modified by:   simonvonschmalensee
* @Last Modified time: 2018-06-25 20:49:23
*/
#include <stdio.h>
#include<stdlib.h>
#define MAX_INPUT_LENGTH 1000
#define PRINT_LENGTH 80

int getline_2(char line[], int maxInputLength);


int main(void){
	int lineLength;
    char *inputLine = malloc(MAX_INPUT_LENGTH*sizeof(char));
    int character;
   
    while((lineLength = getline_2(inputLine,MAX_INPUT_LENGTH))>0){
 	    if(lineLength>PRINT_LENGTH){
 		    printf("%s", inputLine);
        }
     }
}

int getline_2(char line[], int maxInputLength){
    int character, lineLength;

	for (lineLength = 0; lineLength<maxInputLength-1 && (character = getchar())!=EOF && character!='\n';lineLength++)
	{
		line[lineLength] = character;
	}
	if(character == '\n'){
		line[lineLength] = character;
		lineLength++;
	}
	line[lineLength] = '\0';
	return lineLength; 
}

