#include <stdio.h>
#include<stdlib.h>
#define MAX_INPUT_LENGTH 1000
#define PRINT_LENGTH 80

int getline2(char line[], int maxInputLength);
void reverse(char lineToReverse[]);

int main(void){
	int lineLength;
    char *inputLine = malloc(MAX_INPUT_LENGTH*sizeof(char));
    int character;
   
    while((lineLength = getline2(inputLine,MAX_INPUT_LENGTH))>0){
 	   		reverse(inputLine);
 		    printf("%s\n", inputLine);
        }
}

int getline2(char line[], int maxInputLength){
    int character, lineLength;

	for (lineLength = 0; lineLength<maxInputLength-1 && (character = getchar())!=EOF && character!='\n';lineLength++)
	{
		line[lineLength] = character;
	}
	if(character == '\n'){
		line[lineLength] = character;
		lineLength++;
	}
	line[lineLength] = '\0';
	return lineLength; 
}


void reverse(char lineToReverse[]){
	char temp;
	int lineLength=0;
	while(lineToReverse[lineLength]!='\0'){
		lineLength++;
	}


	for(int i = 0; i<lineLength; i++){
		temp = lineToReverse[i];
		lineToReverse[i] = lineToReverse[lineLength-1];
		lineToReverse[lineLength-1] = temp;
		lineLength--;
	} 
} 




