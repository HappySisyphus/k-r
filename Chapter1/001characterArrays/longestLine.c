#include <stdio.h>
#include<stdlib.h>
#define MAX_INPUT_LENGTH 1000

int getline_2(char line[], int maxInputLength);
void copy(char *to, char *from);

int main(void){

	int currentLength;
	int longestLength = 0;
	char *currentLine = malloc(MAX_INPUT_LENGTH*sizeof(char));
	char *longestLine = malloc(MAX_INPUT_LENGTH*sizeof(char));

    
    while ((currentLength = getline_2(currentLine,MAX_INPUT_LENGTH)) > 0){
        if(currentLength>longestLength){
        	longestLength = currentLength;
        	copy(longestLine, currentLine );
        }
    }

    printf("%d %s The string;\n",longestLength,longestLine);
    free(currentLine);
    free(longestLine);

	return 0;
}


int getline_2(char line[], int maxInputLength){

	int character, lineLength;

	for (lineLength = 0; lineLength<maxInputLength-1 && (character = getchar())!=EOF && character!='\n';lineLength++)
	{
		line[lineLength] = character;
	}
	if(character == '\n'){
		line[lineLength] = character;
		lineLength++;
	}
	line[lineLength] = '\0';
	return lineLength; 
}

void copy(char *to, char *from){
	
	int i = 0;
	//Second condition to prevent buffer overflow
	//Why segfault with dynamic array but not static array?
	while((to[i]=from[i]) != '\0' && MAX_INPUT_LENGTH>i){
		i++;
	}
}