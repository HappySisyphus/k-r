#include <stdio.h>
#include <stdlib.h>
#define MAX_INPUT_LENGTH 1000
int getline2(char line[], int maxInputLength);
int getline2RemoveTrailing(char line[], int maxInputLength);
void removeTrailing(char line[]);

int main(void) {
  int lineLength;
  char *inputLine = malloc(MAX_INPUT_LENGTH * sizeof(char));
  int character;

  while ((lineLength = getline2(inputLine, MAX_INPUT_LENGTH)) > 0) {
    removeTrailing(inputLine);
    printf("%s\n", inputLine);
  }
}

int getline2(char line[], int maxInputLength) {
  int character, lineLength;

  for (lineLength = 0; lineLength < maxInputLength - 1 &&
                       (character = getchar()) != EOF && character != '\n';
       lineLength++) {
    line[lineLength] = character;
  }
  if (character == '\n') {
    line[lineLength] = character;
    lineLength++;
  }
  line[lineLength] = '\0';
  return lineLength;
}
// This function does not only remove trailing blanks, it
// removes all blanks that are following a previous blank.
// The string FOO<space><tab>BAR would be returned as FOO<space>BAR.
// The string FOO<tab><space>BAR would be returned as FOO<tab>BAR.
int getline2RemoveTrailing2(char line[], int maxInputLength) {
  int character;
  int lastCharacter = '\t';
  int lineLength = 0;
  while (lineLength < maxInputLength - 1 && (character = getchar()) != EOF &&
         character != '\n') {
    if (!(((lastCharacter == character &&
            (lastCharacter == ' ' || lastCharacter == '\t')) ||
           (lastCharacter == ' ' && character == '\t') ||
           (character == ' ' && lastCharacter == '\t')))) {
      line[lineLength] = character;
      lastCharacter = character;
      lineLength++;
    }
  }

  if (character == '\n') {
    line[lineLength] = character;
    lineLength++;
  }

  line[lineLength] = '\0';
  return lineLength;
}

/*Removes trailing blanks
If the input is only blanks, a empty string will be returned*/
void removeTrailing(char line[]) {
  int lengthToNewLine = 0;
  while (line[lengthToNewLine] != '\n') {
    lengthToNewLine++;
  }

  printf("with trailing:%d\n  ", lengthToNewLine);
  while (line[lengthToNewLine - 1] == '\t' || line[lengthToNewLine - 1] == ' ')
    lengthToNewLine--;
  if (lengthToNewLine > 0) {
    line[lengthToNewLine] = '\n';
    line[lengthToNewLine + 1] = '\0';
  }

  else
    line[lengthToNewLine] = '\0';
  printf("without trailing%d\n ", lengthToNewLine);
}
