/*
* @Author: simonvonschmalensee
* @Date:   2018-06-24 18:27:33
* @Last Modified by:   simonvonschmalensee
* @Last Modified time: 2018-06-24 18:41:59
*/

#include <stdio.h>


float farenheitToCelsius(float fahrenheitValue);

int main(void){
    int step, lower, upper;
    step = 20;
    upper = 300;
    lower = 0;
    float fahrenheit = (float)lower;
     

    while(fahrenheit<=upper){
    	printf("%6.1f\n",farenheitToCelsius(fahrenheit));
    	fahrenheit+=step;
    }

return 0;
}


float farenheitToCelsius(float fahrenheitValue){
    return (5.0/9.0)*(fahrenheitValue-32.0);
}