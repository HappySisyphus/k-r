#include <stdio.h>

//*Varibles for state*//
#define OUTSIDE 0
#define INSIDE 1


int main(void){
   int character;
   int state = 0;

   while((character = getchar()) != EOF){
      if((character == ' ')||(character == '\t')||(character == '\n')){
    		state = OUTSIDE;
    		putchar('\n');
      }
      else{
      	putchar(character);
        state = INSIDE;  
      }

      
   } 
   return 0;

}