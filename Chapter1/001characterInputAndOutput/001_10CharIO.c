#include <stdio.h>

int main(void){
    
    int character;
    while((character = getchar()) != EOF){
    	switch (character){
    	case ('\t'):
            printf("\\");
            printf("t");
            break;
        case ('\b'):
            printf("\\");
            printf("b");
            break;
        case ('\\'):
        	printf("\\");
        	printf("\\");
        default:
            putchar(character);   
            break;
        }
    }
}