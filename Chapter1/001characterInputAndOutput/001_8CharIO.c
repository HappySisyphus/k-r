#include <stdio.h>

int main(void)
{
	int character;
	int numberOfTabsSpacesNewlines = 0;

	while((character = getchar())!=EOF)
	{
		if(character == '\n' || character == '\t' || character == 32){
            numberOfTabsSpacesNewlines++; 
		}
	}
    printf("%d\n", numberOfTabsSpacesNewlines);

}