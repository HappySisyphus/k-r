#include <stdio.h>

int main(void){

	int lastChar, character;
	int numberOfChars = 0;
    while((character = getchar())!=EOF){
        /*Only print if the current and the last character is not
        blankspaces at the same time*/

        if((lastChar != 32) || (character != 32) ){
        	putchar(character);
        	numberOfChars++;
        }
        lastChar = character;
    } 
    printf("%d\n",numberOfChars);

}