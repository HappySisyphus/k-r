#include <stdio.h>

int main(void){
    
    /*c has the type int since it must
    be able to store the value of any
    char and also EOF. EOF is defined as
    a integer value in stdio*/
    int c;
    

    /*The assignment of c can be written
    inside the while-expression.
    != has a higher precendence than =.
    That is why the parenteces must be
    around c = getchar().*/
    while(c = (getchar()!= EOF)){
        printf("%d\n",c);
    }
    printf("EOF %d\n", c);
}