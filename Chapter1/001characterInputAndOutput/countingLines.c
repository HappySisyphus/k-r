#include <stdio.h>

int main(void){
    int character, numberOfNewLines; 
    numberOfNewLines = 0;
    

    while((character = getchar()) != EOF){
    	/*Important to use ' instead of ". Otherwise
    	the newline will be interpeted as a string \n 
    	instead of a character*/
    	if(character == '\n'){
    		++numberOfNewLines;
    	}
    }
    printf("%d\n", numberOfNewLines);

}