#include <stdio.h>

/*Program converts farenheit values to celsius values and
prints the to stdout*/

int main(void)
{
    //*declaring varibles*//
    int fahr, celsius;
    int lower, upper, step;
    
    //*Assigning values to the varibles*//
    lower = 0;
    upper = 300;
    step  = 20;
    //*Assigning the first farenheit value to the lowest*//
    fahr = lower;
    printf("Fahrenheit\tCelsius\n");
    while(fahr <= upper){
        celsius = 5 * (fahr-32)/9;
        printf("%d\t%d\n", fahr, celsius);
        fahr = fahr + step;
    }


    

}


