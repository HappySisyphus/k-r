#include <stdio.h>

int main (void){

    float farenheit, celsius;
    int lower, upper, step;


    lower = 0;
    upper = 100;
    step  = 5;

    celsius = lower;
	printf("celsius\tfarenheit \n");
    while(celsius <= upper)
    {
    	farenheit = ((9.0/5.0)*celsius)+32;
    	printf("%3.0f\t%6.1f\n",celsius,farenheit);
    	celsius += step; 
    }
}