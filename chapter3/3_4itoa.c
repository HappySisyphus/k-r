#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#define TRUE 1
#define FALSE 0
void reverse(char string[]);
void itoaModified(int n, char output[]);
int main(void){
	char s1[1024];
	itoaModified(INT_MIN,s1);
	printf("%s\n",s1);
	printf("%d\n",INT_MIN);

	return 0;
}

void itoaModified(int n, char output[]){
	int i = 0;
	int isNegative = FALSE;
	if(n < 0)
		isNegative = TRUE;
	do{
		output[i++] = '0' + abs(n%10);
	}while(abs(n/=10) > 0 );
	
	if(isNegative)
		output[i++]='-';
	output[i] = '\0';
	reverse(output);

}

void reverse(char string[]){
	int i = 0;
	int j = strlen(string)-1;
	int temp;
	while(i<j){
		temp = string[i];
		string[i] = string[j];
		string[j] = temp;
		j--;
		i++;

	}
}