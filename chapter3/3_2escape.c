/*
 * @Author: simonvonschmalensee
 * @Date:   2018-07-05 14:59:21
 * @Last Modified by:   simonvonschmalensee
 * @Last Modified time: 2018-07-06 10:09:29
 *This contains three diffrent versions of the unescape function. all of them works fine.
 */

#include <stdio.h>
#define TRUE 1
#define FALSE 0
void escape(char inputString[], char outputString[]);
void unescape(char inputString[], char outputString[]);
void unescape2(char inputString[], char outputString[]);
void unescape3(char inputString[], char outputString[]);
int main(void) {
  char string1[100] = "FOO	BAR\nFIZZ\nBUZZ";
  char string2[100];
  char string3[100];
  escape(string1, string2);
  printf("String 1 %s\n", string1);
  // printf("String 2 %s\n:", string2);
  unescape3(string2, string3);
  printf("string 3 %s\n", string3);

  return 0;
}






void unescape3(char inputString[],char outputString[]){
  int isEscapeSequence = FALSE;
  int outputStringIndex = 0;
  for(int i = 0; inputString[i]!='\0'; i++){
    switch(inputString[i]){
      case '\\':
        isEscapeSequence = TRUE;
        break;
      //Falls trough to the default case if it is not in a escape sequence
      case 't': 
        if(isEscapeSequence){
          outputString[outputStringIndex++] = '\t';
          isEscapeSequence = FALSE;
          break;
        }
      case 'n':
        if(isEscapeSequence){
          outputString[outputStringIndex++] = '\n';
          isEscapeSequence = FALSE;
          break;
        }
      default:
        outputString[outputStringIndex++] = inputString[i];
        break;
    } 
    outputString[outputStringIndex] = '\0';


  }
}


void unescape2(char inputString[], char outputString[]) {
  int isEscapeSequence = FALSE;
  char currentCharacter;
  int inputStringIndex, outputStringIndex;
  inputStringIndex = outputStringIndex = 0;
  while ((currentCharacter = inputString[inputStringIndex]) != '\0') {

    switch (currentCharacter) {
    case '\\':
      isEscapeSequence = TRUE;
       currentCharacter = inputString[++inputStringIndex];

      break;
    default:
      outputString[outputStringIndex++] = currentCharacter;
      break;
    }
    if (isEscapeSequence) {
      switch (currentCharacter) {
      case 't':
        outputString[++outputStringIndex] = '\t';
        isEscapeSequence = FALSE;
        break;
      case 'n':
        outputString[++outputStringIndex] = '\n';
        isEscapeSequence = FALSE;
        break;
      }
    }
  }
}

void escape(char inputString[], char outputString[]) {
  int inputStringIndex = 0;
  int outputStringIndex = 0;
  char currentCharacter;
  while ((currentCharacter = inputString[inputStringIndex]) != '\0') {

    switch (currentCharacter) {
    case '\t':
      outputString[outputStringIndex++] = '\\';
      outputString[outputStringIndex++] = 't';
      break;
    case '\n':
      outputString[outputStringIndex++] = '\\';
      outputString[outputStringIndex++] = 'n';
      break;
    default:
      outputString[outputStringIndex++] = currentCharacter;
      break;
      inputStringIndex++;
    }

    inputStringIndex++;
  }
  outputString[outputStringIndex] = '\0';
}

void unescape(char inputString[], char outputString[]) {
  int inputStringIndex = 0;
  int outputStringIndex = 0;
  char currentCharacter;
  while ((currentCharacter = inputString[inputStringIndex]) != '\0') {

    switch (currentCharacter) {
    case '\\':
      inputStringIndex++;
      currentCharacter = inputString[inputStringIndex];
      switch (currentCharacter) {
      case 't':
        outputString[outputStringIndex++] = '\t';
        break;
      case 'n':
        outputString[outputStringIndex++] = '\n';
        break;
      }
      break;
    default:
      outputString[outputStringIndex++] = currentCharacter;
      break;
    }
    inputStringIndex++;
  }

  outputString[outputStringIndex] = '\0';
}
