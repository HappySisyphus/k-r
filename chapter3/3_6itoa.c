#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#define INT_MIN -2147483648
void itoa1(int n, char s[], int fieldSize);
void reverse(char s[]);

int main(void) {
  char s1[1024];
  itoa1(2147483647, s1, 10);
  printf("%s\n", s1);
}

void itoa1(int n, char s[], int fieldSize) {
  int i, sign;

  if ((sign = n) < 0 && n != -2147483648 ) {
    n = -n; // Make the number positive
  }
  i = 0;
  do {
    s[i++] = abs(n % 10) + '0';

  } while (abs(n /= 10) > 0);
  if (sign < 0)
    s[i++] = '-';
  while (i < fieldSize) {
    s[i++] = ' ';
  }
  s[i] = '\0';
  reverse(s);
}

void reverse(char s[]) {
  int i, j, c;
  for (i = 0, j = strlen(s) - 1; i < j; i++, j--) {
    c = s[i], s[i] = s[j], s[j] = c;
  }
}