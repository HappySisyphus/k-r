#include <stdio.h>
#define TRUE 1
#define FALSE 0
void expand(char input[], char output[]);
void expand1(char input[], char output[]);
int main(void) {
  char string1[1024] = "-a-z 0-9 a-d-f -0-2 some text 1-1 WITH CAPITALS! 0-0 5-3 -";
  char string2[1024];
  expand1(string1, string2);
  printf("%s\n", string2);
  return 0;
}

void expand1(char input[], char output[]) {
  int i = 0,j = 0;
  int isShorthand = FALSE;
  int range = 0;
  while (input[i] == '-') {
    output[j++] = '-';
    i++;
  }
  for (; input[i] != '\0'; i++) {

    if (input[i] == '-' && !isShorthand) {
      isShorthand = TRUE;
      if (input[i+1] == '\0' || input[i-1]==' ') {
        output[j++] = '-';
        isShorthand = FALSE;
      }
    }

    else if (input[i] == '-' && isShorthand) {
      isShorthand = FALSE;
      output[j++] = '-';

    }

    else {

      if (isShorthand) {
        range = input[i] - input[i-2];
        isShorthand = FALSE;
        for (int k = 1; k <= range; k++) {
          output[j++] = input[i-2] + k;
        }
      } else
        output[j++] = input[i];
    }
  }
  output[j] = '\0';
}

void expand(char input[], char output[]) {
  int i = 0;
  int j = 0;
  int range;
  char lastChar;

  // Remove trainling -
  while (input[i] == '-') {
    i++;
  }

  while (input[i] != '\0') {

    range = input[i + 2] - input[i];

    for (int k = 0; k <= range; k++) {
      if ((input[i] + k) != lastChar) {
        lastChar = output[j] = input[i] + k;
        j++;
      }
    }
    if (input[i + 3] == '-' && input[i + 4] != '\0') {
      i += 2;
    } else
      i += 3;
  }
  output[j] = '\0';
}