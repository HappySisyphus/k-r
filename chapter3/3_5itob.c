#include <stdio.h>
#include <string.h>
#define TRUE 1
#define FALSE 0
void itoa1(int n, char s[]);
void reverse(char s[]);
void itob(int n, char s[], int base);
void itob1(int n, char s[], int base);
int main(void) {
  char s1[1024], s2[1024];
  itob(2058, s1, 16);
  itob1(2058, s2, 6);
  printf("%s\n", s1);
  printf("%s\n", s2);
  return 0;
}

void itoa1(int n, char s[]) {
  int i, sign;

  if ((sign = n) < 0) {
    n = -n; // Make the number positive
  }
  i = 0;
  do {
    s[i++] = n % 10 + '0';

  } while ((n /= 10) > 0);
  if (sign < 0)
    s[i++] = '-';
  s[i] = '\0';
  reverse(s);
}

void itob(int n, char s[], int base) {
  int i = 0;
  int modulo;
  int sign;
  if ((sign = n) < 0) {
    n = -n;
  }

  do {
    modulo = n % base;
    printf("%d\n", modulo);
    if (modulo >= 10) {
      s[i++] = 'A' + (modulo - 10);
    }

    else
      s[i++] = '0' + modulo;
  } while ((n /= base) > 0);
  if (sign < 0) {
    s[i++] = '-';
  }
  s[i] = '\0';
  reverse(s);
}

void itob1(int n, char s[], int base) {

  int i = 0;
  int fail = 0;
  int isNegative = FALSE;

  if (base < 2 || base > 36) {
    fail = 1;
    goto fail;
  }

  if (n < 0) {
    isNegative = TRUE;
    n = -n;
  }
  char symbols[37] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  do {
    s[i++] = symbols[n % base];
  } while ((n /= base) > 0);

  if (isNegative) {
    s[i++] = '-';
  }
  s[i] = '\0';
  reverse(s);
fail:
  if (fail) {
    printf("ERROR NOT VALID BASE\n");
  }
}

void reverse(char s[]) {
  int i, j, c;
  for (i = 0, j = strlen(s) - 1; i < j; i++, j--) {
    c = s[i], s[i] = s[j], s[j] = c;
  }
}
