#include <stdio.h>
#include <stdlib.h>

int binarySearch(int ValueToBeFound, int arrayToSearch[], int arrayLength);
int main(void) {
  int array[10];
  for (int i = 0; i < 10; i++) {
    array[i] = i;
  }
  for (int i = 0; i < 20; i++) {
    printf("%d\n", binarySearch(i, array, 10));
  }

  return 0;
}

// Binary search function, returns the index of the searched number if it is
// found
// if there are no match, it returns -1
int binarySearch(int ValueToBeFound, int arrayToSearch[], int arrayLength) {
  int high, low, mid;
  high = arrayLength - 1;
  low = 0;
  mid = (high + low) / 2;
  while (high >= low && arrayToSearch[mid] != ValueToBeFound) {

    if (arrayToSearch[mid] > ValueToBeFound) {
      high = mid - 1;
    }

    else {
      low = mid + 1;
    }
    mid = (high + low) / 2;
  }
  return (ValueToBeFound == arrayToSearch[mid]) ? mid : -1;
  // if(arrayToSearch[mid]==ValueToBeFound){return mid;}
  // return -1;
}
